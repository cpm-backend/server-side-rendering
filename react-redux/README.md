# Server Side Rendering with React and Redux

When we visit a website which is built by traditional React app, our browser will request this page. Actually, it's a simple HTML, there are nothing in the body. All the content is provided by the JS file (i.e. `bundle.js`). So we need to wait some time to get that JS file. Then React app boots, it might need to fetch some data from backend. Finally we can see the content in our browser.
<br/><img src="./screenshots/spa-time.png" width="300"/><br/>

The above steps is not efficient. In some specific app, we want to see the content ASAP, we don't want to wait too much time. That's a problem which server side rendering (SSR) resolves.

When user visits a SSR page, user will get a complete HTML from the browser like so:
<br/><img src="./screenshots/ssr-time.png" width="300"/><br/>

You can see complete step in the browser in this figure:
<br/><img src="./screenshots/ssr-1.png" width="500"/><br/>

But the HTML is static, we want to interact with this website. So it should be is a React app, browser still need to load `bundle.js`. The takeaway here is user can get a page quickly.
<br/><img src="./screenshots/ssr-2.png" width="500"/><br/>

## Get Started
We are going to build a simple project to learn SSR. Visit [here](https://react-ssr-api.herokuapp.com/) to see the demo.

There are 4 pages we need to provide, which are homepage, users page, admins page (need to login), and error page.

In this app, we need 2 servers. One is API server, this is our backend server, we store and query data here. Another is render server which is responsible for produce HTML page.
<br/><img src="./screenshots/two-servers.png" width="400"/><br/>

There are 2 common cases in the app. One is complex backend with simple frontend. In this case, we need a lot of API servers to serve our frontend. But we might only need one rendering server. Another opposite case is simple backend with complex frontend. We only need one API server, but we need a lot of rendering servers to produce HTML. That's why we need two types of servers, it's good to decouple.

Notice that SSR is not perfect. Running React on the server is very slow. So we will focus on the performance of our rendering server. In the case of complex frontend with simple backend, we can more easily meet the demands.

You can refer to the [server](server/) project to learn following concepts. Follow the steps:
1. Install some packages.
    ```
    cd server
    yarn
    ```
1. Start this service and visit http://localhost:3000.
    ```
    yarn start
    ```

### Express Server
We use *Express* to be our render server. In traditional React app, we use `render()` method to create instances of a bunch of components and mounts them to a DOM node. But in SSR, we use `renderToString()` to render a bunch of components one time and produces a string out of all the resulting HTML.

But the problem is server side does not know JSX code, you will see error:
```js
import Home from './client/components/Home';

app.get('/', (req, res) => {
  const content = renderToString(<Home />);
  res.send(content);
});
```

Notice that we use ES2015 modules instead of common JS modules in [index.js](server/src/index.js) which is our render server. There is no ES2015 supported by *NodeJS* now, but why we can run these code?

To fix both issues, we need to run *Webpack* on all of our server side code then execute the resulting bundle. We feed JSX and server side code into Webpack and *Babel*. Babel will be responsible for turning all JSX into normal ES5 code and end up with a `bundle.js` file. We use *Node* to run `bundle.js` file.

Check the code in [webpack.server.js](server/webpack.server.js), we can use following code to generate `build/bundle.js`:
```
webpack --config webpack.server.js
```

Then we use *Node* to run this file.
```
node build/bundle.js
```

> ### *Isomorphic Javascript*
> *Isomorphic* (or universal) javascript means the same code style on the server and the browser. For example, we use ES2015 module in both client and server side.

### Second `bundle.js`
As we discussed before, HTML is a static page. If we want to put event handler like `onClick` into HTML, we have to load second `bundle.js` like a normal React app.

So the first `bundle.js` contains all server side and client side code, we run this code on our backend. The second `bundle.js` is React app code, it would be shipped down to the browser.

Why don't we use one `bundle.js`? The answer is easy, because server side code might contain some sensitive information like secret keys. We don't want to ship down this code to browser.

For generating second `bundle.js`, see [webpack.client.js](server/webpack.client.js). Notice that the entry point of client app is different (i.e. `src/client/index.js`) and the output path is `/public`.

We can easily load this second `bundle.js` like so:
```js
app.use(express.static('public'));
app.get('/', (req, res) => {
  const content = renderToString(<Home />);
  const html = `
    <html>
      <head></head>
      <body>
        <div>${content}</div>
        <script src="bundle.js"></script>
      </body>
    </html
  `;

  res.send(html);
});
```

The interesting question is what code should be put into these 2 `bundle.js`? The code which needs to **only** run on the server should be put into server `bundle.js`, the code which needs to **only** run on the browser should be put into client `bundle.js`. **You should never put the server side code in into client `bundle.js`.**

In the browser, we find browser get a HTML which contains a `bundle.js` script first. Then the browser requests this `bundle.js`. The key point is we can get a whole page very quickly, but some functionality might not be ready because our browser still needs sometime to get `bundle.js`. See following figure:
<br/><img src="./screenshots/second-bundlejs.png" width="550"/>

So after the browser loads client `bundle.js`, we manually render the React app a second time into the **same** HTML part. React renders our app on the client side and compares the new HTML to what already exists in the document. Finally, React takes over the existing rendered app, binds event handlers, etc. See the code in [client/index.js](server/src/client/index.js):
```js
ReactDOM.hydrate(<Home />, document.querySelector('#root'));
```

So we render `root` part in HTML which is generated by server side. In other word, `root` part is rendered 2 times. Notice that we use `hydrate()` instead of `render()`, that's because calling `render()` to hydrate server-rendered markup will stop working in React version 17. If you want React to attach to the server HTML, you need to use `hydrate()`.

### Webpack Refactoring
We use `webpack-merge` library to share the same code in [webpack.base.js](server/webpack.base.js) for `webpack.client.js` and `webpack.server.js`.

And for the server side, we don't want to put some client library when generating `bundle.js`. So we use `webpackNodeExternals` library to ignore some files. Of course, this is a minor improvement, because we never ship server `bundle.js` down to browser. But it can make us generate server `bundle.js` faster.

## Navigation
Let's discuss navigation scenario. If user's browser request `/users` page, our express handler of `app.get(*)` responds and send down `index.html` and `bundle.js` files. Then React boots up, *React Router* boots up. `BrowserRouter` looks at URL in address bar to render some route.

The key point is when we render our application on the server, we do not have an address bar present. So cannot use `BrowserRoute` in server. Instead, we can use `StaticRouter`. `StaticRouter` is designed for SSR. Here is our design:
<br/><img src="./screenshots/router.png" width="350"/><br/>

### HTML Mismatch
When you develop SSR app, you might often gets following error message:
<br/><img src="./screenshots/hydrate-error.png" width="250"/><br/>

Imagine that when we render the app on our server we get a `<div>` that contains a `<button>` tag. But when we *hydrate* the app on the client we end up with a `<div>` that instead contain a `<div>`, and this `<div>` contains `<button>`.
```js
// Server
<div>
  <button></button>
</div>
```
```js
// Client
<div>
  <div>
    <button></button>
  </div>
</div>
```

When the hydration step takes place on the client, React expects to see the original server produced HTML and the newly hydrated React app to have an absolutely identical output. If they don't, React going to complain and you will see that error message (i.e. *Warning: Expected server HTML to contain a matching `<div>` in `<div>`*).

This message means that there is a mismatch between the HTML that is produced by our server and the HTML produced by our client. In SSR app, when React renders HTML which produced by server, it might change the HTML content and structure.

To solve this issue, please see [Rehydration](./README.md#rehydration) section.

### Routes
As mentioned before, we need to use `StaticRouter` in server side. See the following code:
```js
app.get('*', (req, res) => {
  res.send(renderer(req));
});

const renderer = (req) => {
  const content = renderToString(
    <StaticRouter location={req.path} context={{}}>
      <Routes />
    </StaticRouter>
  );

  return '<html>...</html>';
};
```

When user visit a specific URL, we can get it from `req.path`. Then when we get this path, we pass it to `<Route>`. `<Route>` will render the component based on this path.

There are 2 tiers of routing. First one is Express routing tier, we don't handle path in this tier (i.e. `app.get('*')`), we pass all traffic to second router, see [index.js](server/src/index.js). The second one is React routing tier, we determine which component shows up in this tier, see [Routes.js](server/src/client/Routes.js).

## Integrating with Redux

There are 4 big challenges when we want to integrate SSR with Redux:
1. Redux needs different configuration on browser nad server.
1. Aspects of authentication needs to be handled on server. Normally this is only on browser.
1. (*Very hard!*) We need some way to detect when action creators finish loading initial data on server. So that we can attempt to render the app to a string and send it back down to the browser (we don't need to know **exact instant** when action creators finish in client side).
1. We need state rehydration on the browser.

### Babel Issue
If we want to fetch data when component is mount, we write the code like so:
```js
class UsersList extends Component {
  componentDidMount() {
    this.props.fetchUsers();
  }
}
```

It would be OK in React app. But when we render this component in server side, we introduce an error message:
<br/><img src="./screenshots/regenerator-runtime.png" width="500"/><br/>

That's because we use `async-await` syntax in `fetchUsers()`, see [actions/index.js](server/src/client/actions/index.js). *Babel* assumes that there is something called a *regenerator runtime* defined inside of our working environment. But we don't so we get that error message.

To fix this issue, we import `babel-polyfill` in client side (i.e. [client/index.js](server/src/client/index.js)) and server side (i.e. [index.js](server/src/index.js)). This module will define some of helper functions that Babel wants to use.

### Data Loading Issue
When we render component in server side, we have to know exact instant  when action creators finish. See the following code:
```js
export default (req, store) => {
  const content = renderToString(<UsersList />);

  return `
    <html>
      <head></head>
      <body>
        <div id="root">${content}</div>
        <script src="bundle.js"></script>
      </body>
    </html>
  `;
};
```

We want to fetch data when `UserListPage` component is mount, but `fetchUsers()` is a async function. In other word, JS won't wait for data fetching in above code. It will send HTML back to browser immediately. That's why we need to know exact instant when action creators finish. We don't want to send HTML back to browser until fetching finishes.

### React Router Config
It's not easy to fix above issue, see our strategy in following figure:
<br/><img src="./screenshots/data-loading.png" width="250"/><br/>

The key point is first 2 steps, we can use `react-router-config` library to achieve our goal. But the bad news is we cannot use `<Route />` component when using `react-router-config`, we have to build route manually. See documentation [here](https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config#route-configuration-shape).

Here is how to implement:
```js
app.get('*', (req, res) => {
  const store = createStore();

  matchRoutes(Routes, req.path).map(({ route }) => {
    return route.loadData ? route.loadData() : null;
  });

  res.send((req, store) => {
    const content = renderToString(
      <Provider store={store}>
        <StaticRouter location={req.path} context={{}}>
          <div>{renderRoutes(Routes)}</div>
        </StaticRouter>
      </Provider>
    );

    return `
      <html>
        <head></head>
        <body>
          <div id="root">${content}</div>
          <script src="bundle.js"></script>
        </body>
      </html>
    `;
  });
});

const Routes = [
  {
    path: '/',
    component: Home,
    exact: true,
  },
  {
    path: '/users',
    component: UsersList,
  },
];
```
```js
// UsersList
class UsersList extends Component {...}

function loadData() {
  console.log('load data');
}

export { loadData };
```
We use `matchRoutes()` function to let server know which component should be rendered. When we get that component, we can call a `loadData()` function attached to each of those components. But the last but not least problem is how to detect all requests complete?

### Store Dispatch
When we call `loadData()` function, we manually dispatch action creator `fetchUsers()`. This function will return a promise. Then we can wait for all promises to resolve and render our app.
<br/><img src="./screenshots/store-dispatch.png" width="400"/><br/>

Here is our implementation:
```js
app.get('*', (req, res) => {
  const store = createStore();

  const promises = matchRoutes(Routes, req.path).map(({ route }) => {
    return route.loadData ? route.loadData(store) : null;
  });

  Promise.all(promises).then(() => {
    res.send(renderer(req, store));
  });
});
```
```js
// UsersList
function loadData(store) {
  return store.dispatch(fetchUsers());
}
```

Let's discuss some interesting topics in above implementation:
- Why don't we use `connect` component for handling the action creator as we usually do? Because `connect` only works through communication with the `Provider`. In our situation, we need to do all this data loading before rendering our app at all. No `connect`, no props or anything like that with `loadData` here because we have not yet rendered our app, which means we cannot use the `connect`.
- Why do we use `dispatch()` inside of `loadData()` function? In our case, we cannot use `connect`, we have to work with Redux manually by dispatching the action creator ourself (`connect` will do this for us).
- Why to use store here? After we get back data from our API, the data gets sent to all of our reducers where assemble our working state object. Finally, our store is full of data. So when we want to render app in server, we have data to do.

To test the content is rendered by server, you can block JS permission in browser:
1. Go to http://localhost:3000/users.
1. Click exclamation mark icon in your address bar.
1. Click **Site settings**.
    <br/><img src="./screenshots/site-setting-1.png" width="300"/>
1. In **Permissions** section, find **JavaScript** and select **Block**.
    <br/><img src="./screenshots/site-setting-2.png" width="600"/>
1. Now you can refresh the page, if you can still see the user list then SSR works. Another way is to check the HTML produced by server in first time.

### Rehydration
After the server Redux fetches data, the **server side Redux store** is full of data so that we can render the page. But unfortunately, the **client side Redux store** is still completely empty when React renders the page or hydrates the existing HTML. This will introduce [HTML mismatch](./README.md#html-mismatch) we discussed before.

So we want to somehow preserve the state that we have prefetch from the server and communicate this state down to the browser.

So we add 2 steps in our process, focus on 2 green boxes in the following figure:
<br/><img src="./screenshots/store-state.png" width="200"/><br/>

After page is rendered on server, the store dumps all state into the HTML template. When client side store is initialized, we can put those state into the store. See the following code in server side:
```js
// Dangerous code!!
app.get('*', (req, res) => {
  const store = createStore();

  Promise.all(promises).then(() => {
    res.send((req, store) => {
      const content = renderToString(...);
      return `
        <html>
          <head></head>
          <body>
            <div id="root">${content}</div>
            <script>
              window.INITIAL_STATE = ${JSON.stringify(store.getState())}
            </script>
            <script src="bundle.js"></script>
          </body>
        </html>
      `; 
    });
  });
});
```

Notice that we get all state and turn it to JSON, set this JSON as a window variable `INITIAL_STATE`. Then we can use this variable in the client side when we initial the store:
```js
const store = createStore(
  reducers,
  window.INITIAL_STATE,   // This argument is for setting up initial value
  applyMiddleware(thunk)
);
```

Yes can pass our state now, but above code is very dangerous! Because we pass server side code directly to browser. The browser might execute malicious code, we call *XSS attack*. See the following malicious data:
```js
[
  {
    id: 1,
    name: "<script><script>alert(123)</script>"
  }
]
```

To prevent XSS attack, we don't want the browser execute the code. All we want browser to do is to show up the data. So we can use `serialize()`, see [renderer.js](server/src/helpers/renderer.js):
```js
return `
  <script>
    window.INITIAL_STATE = ${serialize(store.getState())}
  </script>
`;
```

## Authentication
In traditional app, authentication is like a contract between browser and server. Every requests that a browser makes to the API server, it will include some type of identifying piece of information (e.g. JSON web token, cookie). API server will inspect that information and identify the user right to determine what data should be returned.

But in SSR, Rendering server (i.e. Express server) will be responsible for making API requests during the server rendering phase. After rendering server receives that request, it still need to send a request to API server like user's browser.

There are two jobs rendering server needs to do, first one is that rendering server needs some type of knowledge of exactly who is attempting to access the API. Second one is that rendering server needs to be able to somehow fool the API server into thinking that it is the user's browser.

If we use cookies as authentication method, as soon as we host our API server and the render server on different domains, sub-domains or ports, we will run into an issue. Because all cookies in the browser correspond to the server at the domain, sub-domain, port that issued them. In other word, browser does not send cookies to render server when we want to visit API server.
<br/><img src="./screenshots/cookie-issue.png" width="700"/><br/>

### Proxy
We handle authentication issues in two phases, one is initial page fetching, another is follow-up AJAX requests, see the following figure:
<br/><img src="./screenshots/proxy.png" width="500"/><br/>

So during the initial page load phase, the server needs to have access to some cookie that proves that the original user from the browser is logged in. So we need to manually attach the cookie that got send from the original request, and render server makes a request to API on behalf of browser.

In the second phase, the render server will no longer touch or modify incoming requests. It will simply pass requests onto the API through the use of the *proxy*.

So there are 3 steps we have to do:
1. Set up Proxy server to proxy requests that are being made from the browser to the render server off to the API.
1. Make sure that those exact same action creators that are called later on from the **client** will be sent to the proxy and then to the API server.
1. Make sure that any API requests (i.e. action creators) that we call while rendering our app on the **server** will be sent off directly to our API.

> ### *Why not JWT?*
> You might think about that we can attach JSON web token (JWT) to every request like inside the header or body, why don't we use JWT instead which doesn't follow the cookies restriction?
>
> Because JWT has to be manually attached to any **request** in order to show a protected page. When user enters URL into the address bar and press enter. The browser makes a GET request to that domain and absolutely no information is included along with with this first request except for typical cookies and headers. So the render server would have to turn around and send back to the browser to ask JWT. The browser sends a second request with JWT then we can see the content.
>
> The key point is we have to send 2 requests to get this protected page by JWT, we would no longer be able to send back rendered HTML content as a response to the initial request. That conflicts with SSR solution.
>
> The only thing that we can count on being sent to that domain in the initial request is the cookies that are tied to that domain. So that's why we are using a cookie-based authentication flow here.

Now let's implement code for the step one. To forward traffic to API server, we can set up a proxy like so:
```js
app.use(
  '/api',
  proxy('http://react-ssr-api.herokuapp.com', {
    proxyReqBodyDecorator(opts) {
      opts.header['x-forwarded-host'] = 'localhost:3000';
      return opts;
    },
  })
);
```

Notice that if you have your own API server instead of using the server we provide, you don't need to set up second argument for `proxy()` method. This setting is to tell API server that this is a proxy request and the origin is `localhost:3000`. This API server is set up to look for this header (i.e. `x-forwarded-host`), if it exists it will attempt to redirect the user back to a `localhost:3000` after it goes through the OAuth process.

### API Communication
As we discussed before, let's see our design to compare initial page load and follow-up requests:
<br/><img src="./screenshots/initial-page-load.png" width="650"/>
<br/><img src="./screenshots/follow-up-requests.png" width="700"/><br/>

To handle step 2 and 3, we need to create a custom `axios` instance, see document [here](https://github.com/axios/axios#creating-an-instance). We pass this custom `axios` to Redux Thunk, then custom `axios` is available in action creator.

### Client `axios` Instance
Let's start from client side (step 2), see the following code:
```js
const axiosInstance = axios.create({
  baseURL: '/api',
});

const store = createStore(
  reducers,
  window.INITIAL_STATE,
  applyMiddleware(thunk.withExtraArgument(axiosInstance))
);
```

We create an instance of custom `axios`, notice that we set up a base URL `/api` which means any path will add this prefix. For example, if we write `axios.get('/users')` which means the path is `/api/users`. That's because render server will forward all traffic which path prefix is `/api` to API server.

Another takeaway is we create a special Thunk with extra argument. So now we can use third argument in our action creator, you can see thunk source code as follows:
```js
function createThunkMiddleware(extraArgument) {
  return ({ dispatch, getState }) => (next) => (action) => {
    if (typeof action === 'function') {
      // We can use third argument in action creator if we provide 'extraArgument'
      return action(dispatch, getState, extraArgument);
    }

    return next(action);
  };
}

const thunk = createThunkMiddleware();
// This line is how we create a special Thunk with 'extraArgument'
thunk.withExtraArgument = createThunkMiddleware;

export default thunk;
```

So we can use this custom `axiosInstance` (i.e. `api` in below code) in our action creator like so:
```js
export const fetchUsers = () => async (dispatch, getState, api) => {
  const res = await api.get('/users');
  dispatch(...);
};
```

### Server `axios` Instance
Server side (step 3) is similar to client side. We get the cookie from request and put it into headers, see [createStore.js](server/src/helpers/createStore.js):
```js
export default (req) => {
  const axiosInstance = axios.create({
    baseURL: 'http://react-ssr-api.herokuapp.com',
    headers: { cookie: req.get('cookie') || '' },
  });
  const store = createStore(
    reducers,
    {},
    applyMiddleware(thunk.withExtraArgument(axiosInstance))
  );

  return store;
};
```

We need to share action creator with client side as we mentioned before, that's why we need to set up `baseURL` for the custom `axios` instance.

### Header
We want to put the login button in the web header. So we need to a centralized component to show this content in any route, that's why we created [App.js](server/src/client/App.js). Let's take a look for `Route` component:
```js
export default [
  {
    ...App,
    routes: [
      {
        ...HomePage,
        path: '/',
        exact: true,
      },
      {
        ...UsersListPage,
        path: '/users',
      },
    ],
  },
];
```
We put the `routes` as prop into `App` component, and we plan to create routes in `App` so that `App` can wrap the routes to achieve our goal. See `App` component:
```js
const App = ({ route }) => {
  return (
    <div>
      <Header />
      {renderRoutes(route.routes)}
    </div>
  );
};
```

### Login & Logout
Header providers the status of user login or logout, in other word, we have to know if a user login when we render `App` component. That's why we need to fetch current user status in `App`:
```js
export default {
  component: App,
  loadData: ({ dispatch }) => dispatch(fetchCurrentUser()),
};
```

After fetching current status of this user, the server Redux store has this state, we can use it in `Header` component:
```js
const Header = ({ auth }) => {
  const authButton = auth ? (
    <a href="/api/logout">Logout</a>
  ) : (
    <a href="/api/auth/google">Login</a>
  );

  return <div>{authButton}</div>;
};

function mapStateToProps({ auth }) {
  return { auth };
}

export default connect(mapStateToProps)(Header);
```

Notice that we use `<a>` tag here, because we only use `<Link>` tag when we want to navigate around **inside** of our React app. In above cases, we want the entire browser to render and change the address. Of course, we have to add `/api` because we want it to be proxied through our render server.

## Error Handling

If users try to visit a page which does not exist, we should provide a 404 page for them and their browser should get 404 status.

### Not Found Page
Creating a not found page component is easy, but it's a little bit complicated in SSR. We need to provide `StaticRouter` a `context` argument:
```js
<StaticRouter location={req.path} context={context}>
  <div>{renderRoutes(Routes)}</div>
</StaticRouter>
```

This special argument will pass to all route component in `Routes` as a prop (in `staticContext` field), so we can use it in `NotFoundPage` component like so:
```js
const NotFoundPage = ({ staticContext = {} }) => {
  staticContext.notFound = true;
  return <h1>Ooops, route not found.</h1>;
};
```

Notice that we have to give it a default value, because there is no `staticContext` property in client side routers. So in this component, we can set our property `notFound` to `true`. After server renders, we can get this value to respond 404 status like so:
```js
app.get('*', (req, res) => {
  Promise.all(promises).then(() => {
    ...
    const context = {};
    // After this step, our 'context' with a field 'notFound'
    const content = renderer(req, store, context);

    if (context.notFound) {
      res.status(404);
    }
    res.send(content);
  });
});
```

### Admins Page
Admins page is our protected page, user has to login to see this data. Interesting thing here is what happens when user visits this page without login?

See the following action creator:
```js
export const fetchAdmins = () => async (dispatch, getState, api) => {
  const res = await api.get('/admins');

  dispatch({
    type: FETCH_ADMINS,
    payload: res.data,
  });
};
```

If this user is not authenticated in, we will get error from API which means `api.get('/admins')` will throw error. This error will break our `Promise.all()` in render server because this status is *rejected*. Finally the page will freeze and cannot app is broken.

A possible way to fix this issue is to add `catch()` in promise chain to handle certain promise is not resolved like so:
```js
// Not good!
Promise.all(promises).then(() => {...}).catch(...);
```

But above approach is not so good. Imagine we have 5 components need to render in this page, if one component gets error, we might want other 4 components to show up to users. But if we use above method, we won't wait other 4 components finish instead execute code in `catch()` immediately, that's what we don't want.

So our solution is to wrap all promises which are passed in `Promise.all()` with new `Promise`, so we got 2 layers of promise. Whenever inner promise is resolved or rejected, we will manually resolve the outer promise. So now the outer promise will always be resolved.
```js
const promises = matchRoutes(Routes, req.path)
  .map(({ route }) => {
    return route.loadData ? route.loadData(store) : null;
  })
  .map((promise) => {
    if (promise) {
      return new Promise((resolve, reject) => {
        promise.then(resolve).catch(resolve);
      });
    }
  });

Promise.all(promises).then(...);
```

### Redirect
In order to handle the case in which a users try to visit the admins page when they are not authentication, we need a new component `RequireAuth`. We can use this component to redirect the user or show specific component (i.e. `AdminsListPage`). For this special component, we call *higher order components*.

Notice that we don't handle the errors in server rendering phase. Instead, we handle errors in client side phase. We won't stop the process when render server renders the page.
<br/><img src="./screenshots/handle-time.png" width="500"/><br/>

In [RequireAuth](server/src/client/components/hocs/RequireAuth.js) component, we use `auth` prop to determine which action we should take. If the user is not authenticated, we redirect user to homepage:
```js
render() {
  switch (this.props.auth) {
    case false:
      return <Redirect to="/" />;
    case null:
      return <div>Loading...</div>;
    default:
      return <ChildComponent {...this.props} />;
  }
}
```

To apply this special component, we can put it into routes or admins page. The following code is to wrap admins page with `RequireAuth` component.
```js
export default {
  component: connect(mapStateToProps, { fetchAdmins })(
    requireAuth(AdminsListPage)
  ),
  loadData: ({ dispatch }) => dispatch(fetchAdmins()),
};
```

After return `<Redirect to="/">` component, the `context` has this information. We can make this data to redirect the user:
```js
Promise.all(promises).then(() => {
  const context = {};
  // We call 'RequireAuth' in this step
  const content = renderer(req, store, context);

  // We can redirect user
  if (context.url) {
    return res.redirect(301, context.url);
  }
});
```

## SEO
SEO is an important topic for SSR. The easiest way is to set up meta tags on any HTML inside of your application to give application like FB, LinkedIn, Google and so on the ability to parse the content on your page and put together a very quick description of what your page is all about.

We are going to use [React Helmet](https://github.com/nfl/react-helmet) to render these tags. For normal React app (client side rendering), when users visit a page, we render `Helmet` tag and Helmet takes new tags and manually tinkers with HTML in `head` tag, you can refer to this [example](https://github.com/nfl/react-helmet#example).

But when we render the page on the server, Helmet does not have the ability to reach up to the `head` tag. Because when we render the page on the server, we have none of those browser API is around manipulating the header in place. So we need to dump `Helmet` tags directly into HTML template.

In the following tutorial, we only set up `og:title`. But in real app, we should set up 4 OG tags, check Open Graph protocol (OGP) [here](https://ogp.me/).

We can change title and `meta` tag dynamically, check the code in [UsersListPage.js](server/src/client/pages/UsersListPage.js):
```js
head() {
  return (
    <Helmet>
      <title>{`${this.props.users.length} Users Loaded`}</title>
      <meta property="og:title" content="Users App" />
    </Helmet>
  );
}

render() {
  return (
    <div>
      {this.head()}
    </div>
  );
}
```
Notice that the `<title>` in above code is HTML title, it will show up in your browser tab. It's not an OG title for SEO. Another thing we want to mention is hat we have to use one expression in `Helmet`, so the following code will trigger an error:
```js
<Helmet>
  <title>{this.props.users.length} Users Loaded</title>
</Helmet>
```

In server side, all we need to do is to put the Helmet tags directly into HTML template, see [renderer.js](server/src/helpers/renderer.js):
```js
const helmet = Helmet.renderStatic();

return `
  <html>
    <head>
      ${helmet.title.toString()}
      ${helmet.meta.toString()}
    </head>
  </html>
`;
```

## Other Topics

This section we will discuss some common SSR topics.

### `RenderToNodeStream`
Actually, there are 4 different types of render function in React DOM, see [here](https://reactjs.org/docs/react-dom-server.html). The function `renderToStaticMarkup()` is used for HTML that we don't intent to rehydrate with react on the user's browser, we are not going to use in our case.

But the other 2 functions `renderToNodeStream()` and `renderToStaticNodeStream()`, you might also seen a lot of popular articles about SSR.

When we use `renderToString()`, our server needs to generate whole HTML file and pass to users. If this file is too large, it will take a lot of time. In other word, users need to wait a lot of time, TTFB (Time To First Byte) is large. But if we use `renderToNodeStream()`, we only build some part of HTML then we send back to users, so TTFB is small. See following figure to know how `renderToNodeStream()` works:
<br/><img src="./screenshots/render-to-node-stream.png" width="450"/><br/>

Now you might want to ask, why don't we use this? It can improve our render performance. But actually we can not do this. When user visits some protected page, if we use `renderToNodeStream()`, user will get some part of page until server found that something wrong then stop rendering. In other word, user might seen some part of HTML when visiting protected page even though this user is not authenticated.

### Reminder
If you think that you want to use SSR, you'd better starting from day one on your app. Going back to an existing app and adding in SSR after the fact is extremely challenging.
