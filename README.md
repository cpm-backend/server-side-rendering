# Server Side Rendering
This is a study note for learning server side rendering (SSR) with *React* and *Redux*.

## Reference
[Server Side Rendering with React and Redux](https://www.udemy.com/course/server-side-rendering-with-react-and-redux/).
